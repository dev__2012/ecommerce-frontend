import styled from "styled-components";

const Button = styled.button`
  outline: none;
  padding: 8px 16px;
  font-size: 12pt;
  font-weight: bold;
  text-transform: uppercase;
  border: none;
  border-radius: 5px;
  background: #756c63;
  color: white;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  cursor: pointer;

  &:active {
    filter: brightness(80%);
  }

  &:disabled {
    filter: brightness(75%);
    border: none;
    cursor: default;
  }
`;

export default Button;
