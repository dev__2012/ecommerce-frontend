import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

export type NavBarItemProps = {
  text: string;
  icon?: JSX.Element;
  align?: "left" | "right";
};

export type NavBarItemIconProps = {
  icon: IconDefinition;
};
