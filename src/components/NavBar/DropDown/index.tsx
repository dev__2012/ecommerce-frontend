import React from 'react';
import styled from 'styled-components';
import { DropDownContainerProps, DropDownProps } from './types';

const DropDownWrapper = styled.div`
  display: inline-block;
  position: relative;
`;

const DropDownContainer = styled.div<DropDownContainerProps>`
  display: ${(props) => (props.hovered ? "flex" : "none")};
  position: absolute;
  flex-direction: column;
  background: #f9f9f9;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  transition: height 0.5s;
  min-width: 160px;
  z-index: 1;
  ${(props) => (props.align === "right" ? "right: 0" : "")};
`;

const DropDownItem = styled.a`
  text-decoration: none;
  text-align: left;
  color: black;
  font-size: 10pt;
  cursor: pointer;
  padding: 12px 16px;

  &:hover {
    background: #f1f1f1;
  }
`;

const DropDown = (props: DropDownProps) => {
  const [hovered, setHovered] = React.useState<boolean>(false);

  return (
    <DropDownWrapper
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      {props.children}
      <DropDownContainer hovered={hovered} align={props.align}>
        {props.items.map((item) => (
          <>
            <DropDownItem href={item.href} onClick={item.action}>
              {item.text}
            </DropDownItem>
          </>
        ))}
      </DropDownContainer>
    </DropDownWrapper>
  );
};

export default DropDown;
