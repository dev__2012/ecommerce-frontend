import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

export type DropDownItem = {
  text: string;
  icon?: IconDefinition;
  action?: () => void;
  href?: string;
};

export type DropDownContainerProps = {
  hovered: boolean;
  align?: "left" | "right";
};

export type DropDownProps = React.ComponentProps<"div"> & {
  items: DropDownItem[];
  align?: "left" | "right";
};
