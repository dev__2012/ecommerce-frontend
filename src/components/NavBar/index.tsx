import DropDown from "./DropDown";
import styled from "styled-components";
import { faHome, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavBarItemIconProps, NavBarItemProps } from "./types";
import Authentication from "../../api/actions/auth";
import history from "../../routes/history";

const { storage } = Authentication;

const Main = styled.div`
  display: flex;
  justify-content: start;
  background: #423930;
  padding: 0 150px 0 150px;
`;

const NavBarItemContainer = styled.div`
  display: inline-block;
  padding: 14px 16px;
  cursor: pointer;
  transition: background-color 0.3s;

  &:hover {
    background: #756c63;
  }
`;

const NavBarItemText = styled.span`
  text-decoration: none;
  font-size: 12pt;
  color: #ecece2;
  text-align: center;
`;

const NavBarIcon = styled(FontAwesomeIcon)`
  margin-right: 10px;
`;

const NavBarItemIcon = (props: NavBarItemIconProps) => (
  <NavBarIcon icon={props.icon} size="sm" color="white"></NavBarIcon>
);

const NavBarItem = (props: NavBarItemProps) => (
  <NavBarItemContainer
    style={{ marginLeft: props.align === "right" ? "auto" : "default" }}
  >
    {props.icon}
    <NavBarItemText>{props.text}</NavBarItemText>
  </NavBarItemContainer>
);

const AlignRight = styled.span`
  margin-left: auto;
`;

const NavBar = () => {
  return (
    <Main>
      <NavBarItem icon={<NavBarItemIcon icon={faHome} />} text="Home" />
      <NavBarItem text="About" />

      <AlignRight>
        <DropDown
          items={[
            {
              text: "Cart",
              action: () => {
                alert("Open cart!");
              },
            },
            {
              text: "Logout",
              action: () => {
                storage.clean();
                history.push("/auth");
              },
            },
          ]}
          align="right"
        >
          <NavBarItem icon={<NavBarItemIcon icon={faUser} />} text="Profile" />
        </DropDown>
      </AlignRight>
    </Main>
  );
};

export default NavBar;
