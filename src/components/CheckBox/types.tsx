export type ContainerProps = {
  checked?: boolean;
};

export type CheckBoxProps = {
  label: string;
  name?: string;
};
