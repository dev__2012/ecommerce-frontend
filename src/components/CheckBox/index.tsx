import React from "react";
import styled from "styled-components";
import { CheckBoxProps, ContainerProps } from "./types";

const Container = styled.div<ContainerProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px;
  border-radius: 5px;
  background: ${(props) => (props.checked ? "green" : "white")};
`;

const Label = styled.label``;

const Input = styled.input.attrs({ type: "checkbox" })``;

const CheckBox = (props: CheckBoxProps) => {
  const [checked, check] = React.useState<boolean>(false);

  return (
    <Container checked={checked}>
      <Input name={props.name} onChange={() => check(!checked)} />
      <Label>{props.label}</Label>
    </Container>
  );
};

export default CheckBox;
