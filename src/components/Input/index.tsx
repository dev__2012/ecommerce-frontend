import styled from "styled-components";

const Input = styled.input`
  outline: none;
  box-sizing: border-box;
  padding: 10px 16px;
  color: white;
  background: #556672;
  border: none;
  border-radius: 5px;
  width: 100%;
  font-size: 12pt;

  &::placeholder {
    font-size: 12pt;
    color: #fffb;
  }
`;

export default Input;
