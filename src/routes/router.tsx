import { History } from 'history';
import {
    ReactElement,
    ReactNode,
    useLayoutEffect,
    useState
    } from 'react';
import { Router } from 'react-router-dom';

type CustomRouterProps = {
  basename?: string;
  history: History;
  children: ReactNode;
};

const CustomRouter: (props: CustomRouterProps) => ReactElement = ({
  basename,
  children,
  history,
}) => {
  const [state, setState] = useState({
    action: history.action,
    location: history.location,
  });

  useLayoutEffect(() => {
    history.listen(setState);
  }, [history]);

  return (
    <Router
      location={state.location}
      basename={basename}
      children={children}
      navigationType={state.action}
      navigator={history}
    />
  );
};

export default CustomRouter;
