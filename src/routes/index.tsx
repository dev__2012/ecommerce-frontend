import { Home, NotFound } from "../pages";
import { Route, Routes } from "react-router-dom";
import Auth from "../pages/Auth";

const AppRoutes = () => (
  <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/auth" element={<Auth />} />
    <Route path="*" element={<NotFound />} />
  </Routes>
);

export default AppRoutes;
