import styled from 'styled-components';

const Main = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;

const NotFound = () => {
  return (
    <Main>
      <h2>Page Not Found!</h2>
    </Main>
  );
};

export default NotFound;
