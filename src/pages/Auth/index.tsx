import Button from "../../components/Button";
import Input from "../../components/Input";
import React from "react";
import styled from "styled-components";
import { faLock, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Authentication from "../../api/actions/auth";
import history from "../../routes/history";
import CheckBox from "../../components/CheckBox";

const { storage } = Authentication;

const Main = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;

const LoginForm = styled.form`
  display: flex;
  justify-content: start;
  align-items: center;
  flex-direction: column;
  border-radius: 5px;
  padding: 18px 20px;
  min-width: 300px;
  background: #ecece2;
  box-shadow: 0px 8px 16px 4px rgba(0, 0, 0, 0.2);
`;

const LoginFormHeader = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
  flex-direction: row;
  border-bottom: 1px solid black;
  width: 100%;
  padding-bottom: 10px;
`;

const InputSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: start;
  flex-direction: column;
  padding: 18px 20px;
  gap: 15px;
  width: 100%;
`;

const SubmitSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  padding: 6px 10px;
  gap: 5px;
  width: 100%;
`;

const InputIconContainer = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  align-items: center;
`;

const InputIcon = styled(FontAwesomeIcon)`
  position: absolute;
  color: white;
  display: block;
  margin-left: 12px;
`;

const InputWithIcon = styled(Input)`
  padding-left: 38px;
`;

const Auth = () => {
  return (
    <Main>
      <LoginForm
        onSubmit={async (e) => {
          e.preventDefault();

          const formData = new FormData(e.currentTarget);

          const tokens = await Authentication.authorize(
            (formData.get("email") as string) ?? "",
            (formData.get("password") as string) ?? ""
          );

          localStorage.setItem(
            "rememberme",
            formData.get("rememberme") === "on" ? "true" : "false"
          );

          storage.clean();

          storage.store(tokens.access, "access");
          storage.store(tokens.refresh, "refresh");

          history.push("/");
        }}
      >
        <LoginFormHeader>
          <span>Authentication</span>
        </LoginFormHeader>
        <InputSection>
          <InputIconContainer>
            <InputIcon icon={faUser} />
            <InputWithIcon name="email" placeholder="E-mail" type="email" />
          </InputIconContainer>
          <InputIconContainer>
            <InputIcon icon={faLock} />
            <InputWithIcon
              name="password"
              placeholder="Password"
              type="password"
            />
          </InputIconContainer>
        </InputSection>
        <SubmitSection>
          <Button style={{ marginRight: "auto" }} type="submit">
            Sign In
          </Button>
          <CheckBox name="rememberme" label="Remember-me" />
        </SubmitSection>
      </LoginForm>
    </Main>
  );
};

export default Auth;
