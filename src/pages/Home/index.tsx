import Products from '../../api/actions/products';
import React from 'react';
import { NavBar } from '../../components';
import { Product } from '../../api/actions/products/types';

const Home = () => {
  const [products, setProducts] = React.useState<Product[]>([]);

  React.useEffect(() => {
    Products.get().then((prods) => {
      setProducts(prods);
    });
  }, []);

  return (
    <div style={{whiteSpace: "pre"}}>
      <NavBar />
      {products.map((product) => JSON.stringify(product, null, 4))}
    </div>
  );
};

export default Home;
