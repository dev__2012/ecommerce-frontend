import axios, { AxiosError } from "axios";
import history from "../routes/history";
import Authentication from "./actions/auth";

export const apiClient = axios.create({
  baseURL:
    process.env.NODE_ENV === "development"
      ? "http://localhost:8000/api"
      : "/api",
});

apiClient.interceptors.request.use(
  (config) => {
    const { access } = Authentication.storage.tokens();

    if (!access) {
      history.push("/auth");
      return config;
    }

    if (config.headers) {
      config.headers.Authorization = `Bearer ${access}`;
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

apiClient.interceptors.response.use(
  (response) => response,
  async (error: AxiosError) => {
    const originalRequest = error.config;

    if (
      [401, 400].includes(error.response?.status ?? 0) &&
      originalRequest.url?.includes("/auth/refresh/")
    ) {
      Authentication.storage.clean();
      history.push("/auth");
      return Promise.reject(error);
    } else if (error.response?.status === 401) {
      const { refresh } = Authentication.storage.tokens();

      const newAccess = await Authentication.refreshAccessToken(refresh);

      Authentication.storage.store(newAccess, "access");

      return axios(originalRequest);
    }

    alert(JSON.stringify(error.response?.data ?? error.message));
  }
);
