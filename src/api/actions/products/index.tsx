import { apiClient } from "../..";
import { Product } from "./types";

const Products = {
  get: async () => {
    const res = await apiClient.get<Product[]>("/store/products/");

    return res.data;
  },
};

export default Products;
