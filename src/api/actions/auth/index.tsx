import { apiClient } from "../..";
import { JWTToken } from "./types";

const Authentication = {
  authorize: async (email: string, password: string) => {
    const res = await apiClient.post<JWTToken>("/auth/token/", {
      email,
      password,
    });

    return res.data;
  },
  refreshAccessToken: async (refresh: string) => {
    const res = await apiClient.post<JWTToken>("/auth/refresh/", {
      refresh,
    });

    return res.data?.access;
  },
  storage: {
    toUse: () => {
      const rememberMe = localStorage.getItem("rememberme");

      if (rememberMe === "true") return localStorage;

      return sessionStorage;
    },
    tokens: () => {
      const storage = Authentication.storage.toUse();

      return {
        access: storage.getItem("access") ?? "",
        refresh: storage.getItem("refresh") ?? "",
      };
    },
    clean: () => {
      localStorage.removeItem("access");
      sessionStorage.removeItem("access");

      localStorage.removeItem("refresh");
      sessionStorage.removeItem("refresh");
    },
    store: (token: string, tokenType: "refresh" | "access") => {
      const storage = Authentication.storage.toUse();

      storage.setItem(tokenType, token)
    }
  },
};

export default Authentication;
