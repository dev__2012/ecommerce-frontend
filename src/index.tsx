import AppRoutes from "./routes";
import CustomRouter from "./routes/router";
import history from "./routes/history";
import ReactDOM from "react-dom/client";
import "./index.css";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <CustomRouter history={history}>
    <AppRoutes />
  </CustomRouter>
);
