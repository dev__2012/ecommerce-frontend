FROM node AS build
WORKDIR /app

COPY . .

RUN npm install
RUN npm run build

# Proxy Server
FROM nginx
COPY --from=build /app/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]